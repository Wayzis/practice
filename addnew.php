<!DOCTYPE html>

<html>
	<head>
		<meta charset="utf-8"/>
		<title>Добавьте свою картинку!</title>
		<link rel="stylesheet" type="text/css" href="default.css"/>
		<script type="text/javascript">
			function validate(file)
			{
				if (file.files[0].type != "image/jpeg")
				{
					alert("Выбранный файл должен быть .jpg или .jpeg изображением.");
					file.value = "";
				}
			}
		</script>
	</head>
	<body>
		<form id="form" method="POST" enctype="multipart/form-data" action="form.php">
			<label for="image">Изображение:</label>
			<input type="file" name="image" accept=".jpg,.jpeg" onchange="validate(this);" required> <br/>
			<label for="text">Текст:</label>
			<input type="text" name="text" minlength="1" maxlength="255" required> <br/>
			<label for="font">Шрифт текста:</label>
			<select form="form" name="font">
				<option value="consola" selected>Consolas</option>
				<option value="georgia">Georgia</option>
				<option value="tahoma">Tahoma</option>
			</select> <br/>
			<label for="color">Цвет текста:</label>
			<input type="color" name="color" required> <br/>
			<input type="submit" value="Отправить"> <br/>
		</form>
	</body>
</html>
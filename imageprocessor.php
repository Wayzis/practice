<?php
/*
 * Представляет класс изображения с подписью
 */
class SignedImage
{
	public $path = null; //Путь к изображению
	public $text = null; //Подпись изображения
	public $font = null;
	public $color = null;
	public $width = 500; //Ширина изображения
	public $resImage = null;
	function process()
	{
		//Загруженное изображение
		$image = imagecreatefromjpeg('images\\'.$this->path.'.jpg');
		//Шрифт текста
		//(не работает с относительным путём; путь не должен содержать пробелов)
		$fontFull = realpath('fonts\\'.$this->font.'.ttf');
		//Получение размеров изображения
		$imgW = imagesx($image);
		$imgH = imagesy($image);
		$height = ($this->width / $imgW) * $imgH;
		//Новое изображение с изменённым размером
		$this->resImage = imagecreatetruecolor($this->width, $height);
		//Цвет текста
		$hex = hexdec($this->color);
		$red = $hex >> 16;
		$green = ($hex >> 8) & 0xFF;
		$blue = $hex & 0xFF;
		$textcolor = imagecolorallocate($this->resImage, $red, $green, $blue);
		//Закрытие соединения с БД
		$statement = null;
		$dbh = null;
		//Изменение размера изображения
		imagecopyresampled($this->resImage, $image, 0, 0, 0, 0, $this->width, $height, $imgW, $imgH);
		//Положение текста
		$size = 30;
		$angle = 0;
		$box = imagettfbbox($size, $angle, $fontFull, $this->text);
		$x = ($this->width / 2) + (($box[0] - $box[2]) / 2);
		$y = $height + $box[5];
		//Отрисовка текста на изображении
		imagettftext($this->resImage, $size, $angle, $x, $y, $textcolor, $fontFull, $this->text);
	}
	function toBase64()
	{
		ob_start(); //Включение буферизации вывода
		imagejpeg($this->resImage, null, 100); //Запись изображения в буфер
		//Вывод содержимого буфера в кодировке BASE64 через элемент img
		return base64_encode(ob_get_clean());
	}
	function __destruct()
	{
		imagedestroy($this->resImage);
	}
}
?>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="utf-8"/>
		<title>Картинка</title>
		<link rel="stylesheet" type="text/css" href="default.css"/>
	</head>
	<body>
		<!-- Facebook JS SDK -->
		<div id="fb-root"></div>
		<script async src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v7.0"></script>
		<!-- Twitter JS API -->
		<script async src="https://platform.twitter.com/widgets.js"></script>
		<!-- end of scripts -->
		<div style="width: 500px;">
			<?php
				require_once 'dbhandler.php';
				$id = is_numeric($_GET['id']) ? intval($_GET['id']) : null;
				$collection = new ImagesDB();
				$result = $collection->getEntryById($id);
				if (!$result)
				{
					if ($_GET['id'])
					{
						echo '<h1>Такого изображения не существует.</h1><a href="/">Вернуться на главную</a>';
						exit;
					}
					//Случайное изображение, если не указан или указан не корректный идентификатор
					else
						$result = $collection->getEntryByRandom();
				}
				//Ссылка на данную страницу для распространения
				$link = 'http://'.$_SERVER['SERVER_NAME'].'/?id='.$result['ID'];
				require_once 'imageprocessor.php';
				$image = new SignedImage();
				$image->path = $result['PATH'];
				$image->text = $result['TEXT'];
				$image->font = $result['T_FONT'];
				$image->color = $result['T_COLOR'];
				$image->width = 500;
				$image->process();
				echo '<img src="data:image/jpeg;base64,'.$image->toBase64().'"/>';
			?>
			<div class="share">
				<div class="fb-share-button" data-href="<?=$link?>" data-layout="button" data-size="small">
					<a target="_blank"
					href="https://www.facebook.com/sharer/sharer.php?u=<?=urlencode($link)?>&amp;src=sdkpreparse"
					class="fb-xfbml-parse-ignore">Поделиться в Facebook</a>
				</div>
				<a class="twitter-share-button"
				href="https://twitter.com/intent/tweet?url=<?=$link?>"
				data-size="large">
				Tweet</a>
				<a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?=$link?>."
				title="Share by Email">
				<img src="http://png-2.findicons.com/files/icons/573/must_have/48/mail.png" alt="Поделиться по e-mail"/>
				</a>
			</div>
		</div>
	</body>
</html>
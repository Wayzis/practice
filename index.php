<?php
$rules = array(
	'' => 'main',
	'image' => 'image',
	'addnew' => 'addnew',
	'list' => 'list',
);
$uri = $_SERVER['REQUEST_URI'];
$qmark = strpos($uri, '?');
if ($qmark)
	$page = substr($uri, 1, $qmark - 1);
else
	$page = substr($uri, 1);
$file = 'missed';
foreach ($rules as $pattern=>$route)
{
	if (preg_match('#^'.$pattern.'$#', $page))
	{
		$file = $route;
		break;
	}
}
include $file.'.php';
?>
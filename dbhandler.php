<?php
require_once 'config.php';
/*
 * Класс-обёртка для манипуляций с базой данных
 */
class ImagesDB
{
	private $dbh = null;
	public function __construct()
	{
		$this->dbh = new PDO(DB_CONNECTION, DB_NAME, DB_PASS);
	}
	/*
	 * Добавляет новую запись и возвращает её ID
	 */
	public function addEntry($name, $text, $font, $color)
	{
		$statement = $this->dbh->prepare(
			'INSERT INTO PICTURES (PATH, TEXT, T_FONT, T_COLOR)
			VALUES (:path, :text, :font, :color)',
			array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$statement->execute(array(
			':path' => $name,
			':text' => $text,
			':font' => $font,
			':color' => $color));
		$statement = null;
		return $this->dbh->lastInsertId();
	}
	/*
	 * Получает запись по заданному ID
	 */
	public function getEntryById($id)
	{
		$statement = $this->dbh->prepare(
			'SELECT *
			FROM PICTURES
			WHERE ID = :id',
			array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$statement->execute(array(':id' => $id));
		$result = $statement->fetch();
		$statement = null;
		return $result;
	}
	/*
	 * Получает случайную запись
	 */
	public function getEntryByRandom()
	{
		return $this->dbh->query(
			'SELECT *
			FROM PICTURES
			ORDER BY RAND()
			LIMIT 1')->fetch();
	}
	/*
	 * Возвращает количество записей в БД
	 */
	public function count()
	{
		return $this->dbh->query(
			'SELECT count(ID)
			FROM PICTURES')->fetch()[0];
	}
	/*
	 * Получает записи в заданном промежутке
	 */
	public function getEntriesRange($start, $count)
	{
		return $this->dbh->query(
			'SELECT *
			FROM PICTURES
			LIMIT '.intval($start).', '.intval($count))->fetchAll();
	}
	public function __destruct()
	{
		$this->dbh = null;
	}
}
?>
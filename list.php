<!DOCTYPE html>

<html>
	<head>
		<meta charset="utf-8"/>
		<title>Список всех элементов</title>
		<link rel="stylesheet" type="text/css" href="default.css"/>
	</head>
	<body>
		<h1>Здесь приводятся все сохранённые изображения:</h1>
		<table>
			<?php
				const LINES_PER_PAGE = 20;
				$page = $_GET['page'] ?: 1;
				require_once 'dbhandler.php';
				$collection = new ImagesDB();
				$array = $collection->getEntriesRange(
					LINES_PER_PAGE * ($page - 1), LINES_PER_PAGE);
				echo
					'<tr>'.
					'<td>Идентификатор</td>'.
					'<td>Имя</td>'.
					'<td>Текст</td>'.
					'<td>Шрифт</td>'.
					'<td>Цвет</td>'.
					'</tr>';
				for ($i = 0; $i < LINES_PER_PAGE; $i++)
				{
					echo
						'<tr>'.
						'<td>'.($array[$i][0]).'</td>'.
						'<td><a href="/image?id='.$array[$i][0].'" target="_blank">'.htmlspecialchars($array[$i][1]).'</a></td>'.
						'<td>'.htmlspecialchars($array[$i][2]).'</td>'.
						'<td>'.$array[$i][3].'</td>'.
						'<td>'.$array[$i][4].'</td>'.
						'</tr>';
				}
			?>
		</table>
		<?php
			echo '<span class="link">';
			if ($page > 1)
				echo '<a href="/list?page='.($page - 1).'">Предыдущая страница</a>';
			echo '</span><span class="link">';
			if ($collection->count() / LINES_PER_PAGE > $page)
				echo '<a href="/list?page='.($page + 1).'">Следующая страница</a>';
			echo '</span>';
		?>
	</body>
</html>
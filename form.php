<?php
	$errmsg = '';
	//Проверка на поля формы
	if (!$_POST['text'] || !$_POST['font'] || !$_POST['color'] || !$_FILES['image'])
		$errmsg = 'Форма была заполненна некорректно, пожалуйста повторите попытку.';
	else
	{
		$image = $_FILES['image'];
		//Проверка на MIME-тип файла
		if ($image['type'] != 'image/jpeg')
			$errmsg = 'Файл не соответствует MIME-типу "image/jpeg".';
		else
		{
			$filepath = 'images/'.basename($image['name']);
			$check = getimagesize($image['tmp_name']);
		}
	}
	//Проверка на целостность файла
	if ($errmsg == '' && !$check)
		$errmsg = 'Файл повреждён.';
	//Проверка на уникальность имени файла
	if ($errmsg == '' && file_exists($filepath))
		$errmsg = 'Файл с таким названием уже существует.';
	//Проверка возможности перенести изображение в директорию с изображениями
	if ($errmsg == '' && !move_uploaded_file($image['tmp_name'], $filepath))
		$errmsg = 'Файл не может быть загружен.';
	//При ошибке отображаю её на странице
	if ($errmsg != '')
	{?>
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8"/>
				<title>Ошибка</title>
				<link rel="stylesheet" type="text/css" href="default.css"/>
			</head>
			<body>
				<h1><?=$errmsg?></h1>
				<a href="/">Вернуться на главную</a>
			</body>
		</html>
	<?}
	else
	{
		require_once 'dbhandler.php';
		$collection = new ImagesDB();
		$filename = basename($filepath, '.'.pathinfo($filepath, PATHINFO_EXTENSION));
		$id = $collection->addEntry(
			$filename,
			$_POST['text'],
			$_POST['font'],
			substr($_POST['color'], 1));
		//Перенаправление на страницу с загруженной картинкой
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/image?id='.$id);
	}
?>